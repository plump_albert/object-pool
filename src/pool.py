# vim:ft=python:ts=4:sw=0
import worker
import threading

class worker_pool:
    __workers: list[tuple[worker.worker, bool]]
    __sync: threading.Semaphore

    def __init__(self, worker_count=4):
        self.__workers = [ (worker.worker(), False) for _ in range(worker_count) ]
        self.__sync = threading.Semaphore(value=worker_count)

    def get_worker(self) -> worker.worker:
        self.__sync.acquire()
        while True:
            for i, (worker, busy) in enumerate(self.__workers):
                if not busy:
                    self.__workers[i] = (worker, True)
                    return worker

    def return_worker(self, worker) -> None:
        worker.clear()
        for i, [w, busy] in enumerate(self.__workers):
            if busy and w == worker:
                self.__workers[i] = (worker, False)
                self.__sync.release()
                return

    def close(self) -> None:
        for w, _ in self.__workers:
            w.destroy()
