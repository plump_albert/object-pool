# vim:ft=python:ts=4:sw=0
import threading
import time

class worker:
    __stopped: bool
    __thread: threading.Thread
    __thread_lock: threading.Lock

    def __init__(self):
        self.__stopped = False
        self.__thread = threading.Thread(target=self.__thread_main, args=[self])
        self.__thread_lock = threading.Lock()
        self.__task = None
        self.__thread_lock.acquire()
        self.__thread.start()

    def __thread_main(self, this):
        while True:
            this.__thread_lock.acquire()
            this.__thread_lock.release()
            if (this.__stopped):
                return
            this.__task[0](*this.__task[1])

    def perform(self, function, *args):
        self.__task = (function, args)
        self.__thread_lock.release()
        time.sleep(0.1)
        self.__thread_lock.acquire()

    def clear(self):
        self.__task = None

    def destroy(self):
        self.__task = None
        self.__stopped = True
        self.__thread_lock.release()
