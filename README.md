# Лабораторная работа № 1
Реализовать менеджер для загрузки файлов с разделяемого сетевого ресурса
(файл-сервера) с поддержкой многопоточной загрузки. Пользователь задает
URI-адреса нескольких файлов, которые требуется загрузить. Для загрузки
используется заданное количество объектов (потоков) -- не более 1 потока на 1
файл. Должна поддерживаться авторизация для доступа к файлам.

# Thoughts of a dying autist
-[x] create `worker` class that would handle users' jobs
	- has it's own thread
	- using python's `Lock` to wait for work and results
-[x] create pool of those `worker`s and provide an interface of accessing them
-[x] simple http front for user interaction
